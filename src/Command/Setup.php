<?php

namespace Genesii\Wordpress\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

#[AsCommand(
    name: 'setup',
    description: 'Génère les configurations et les fichiers nécessaires au fonctionnement du site.',
    hidden: false
)]
class Setup extends Command {

    protected $vendor = null;
    protected $fs;

    public function __construct(?string $vendor) 
    {
        $this->vendor = $vendor;
        $this->fs = new Filesystem();

        parent::__construct();
    }

    public function configure(): void
    {
        $this
            ->addArgument('composerArgument1', InputArgument::OPTIONAL)
            ->addArgument('composerArgument2', InputArgument::OPTIONAL)
            ->addArgument('composerArgument3', InputArgument::OPTIONAL)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->title('Assistant d\'installation');

        $this->fs->remove([
            $this->vendor,
            $this->vendor . '/../composer.json',
            $this->vendor . '/../composer.lock',
            $this->vendor . '/../src',
        ]);

        $io->writeln('Site installé ! Vous pouvez à présent créer un thème ou des plugins.');
        $io->writeln('');
        $io->writeln('> Pour générer un thème, lancez un <fg=yellow>composer create-project genesii/wordpress-theme</> dans le dossier <fg=yellow>wp-content/themes</>.');
        $io->writeln('> Pour générer un plugin, lancez un <fg=yellow>composer create-project genesii/wordpress-module</> dans le dossier <fg=yellow>wp-content/plugins</>.');
        $io->writeln('');

        return Command::SUCCESS;
    }
}